package com.example.ruben.retrofit;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by rubenmelchers on 18/04/2017.
 */

public class PhotoActivity extends Activity {

    private Uri fileUri;
    Uri selectedImage;
    Bitmap photo;
    String picturePath;
    String ba1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);

            clickPic();

    }

    public void checkPic(View view) {
        clickPic();
    }


    public void clickPic() {

//        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
//        int writePermissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
//        int readPermissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

//        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(
//                    this,
//                    new String[]{Manifest.permission.CAMERA}, 123);
//
//        } else if (writePermissionCheck != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(
//                    this,
//                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 321);
//
//        } else if (readPermissionCheck != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(
//                    this,
//                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 111);
//        } else {

            // Check Camera
            if (getApplicationContext().getPackageManager().hasSystemFeature(
                    PackageManager.FEATURE_CAMERA)) {
                // Open default camera
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                Intent intent2 = new Intent("android.media.action.IMAGE_CAPTURE");
                File file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");

                intent2.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));

                startActivityForResult(intent2, 101);

            } else {
                Toast.makeText(getApplication(), "Camera not supported", Toast.LENGTH_LONG).show();
            }
//        }
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        switch (requestCode) {
//
//            case 123:
//                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
//                    clickPic();
//                } else {
//                    Log.d("TAG", "Call Permission Not Granted");
//                }
//                break;
//
//            case 321:
//                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
//                    clickPic();
//                } else {
//                    Log.d("TAG", "write Permission Not Granted");
//                }
//                break;
//
//            case 111:
//                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
//                    clickPic();
//                } else {
//                    Log.d("TAG", "read Permission Not Granted");
//                }
//                break;
//
//            default:
//                break;
//        }
//    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 101 && resultCode == RESULT_OK) {
            Bitmap bmp = null;


            File file = new File(Environment.getExternalStorageDirectory()+ File.separator + "image.jpg");


            Log.d("file_PHOTO", "FILE PHOTO CONTENT: " + photo);
            Log.d("file_PHOTO", "FULLSIZE FILE PHOTO CONTENT: " + file);

            Log.d("file_PHOTO", "BITMAP PHOTO CONTENT: " + file.getAbsolutePath());

            try {

                //Pop intent
                Intent in1 = new Intent(this, CreationActivity.class);

                in1.putExtra("image", Uri.fromFile(file));
                finish();
                startActivity(in1);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float)height / (float)reqHeight);
        }

        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth) {
            inSampleSize = Math.round((float)width / (float)reqWidth);
        }

        options.inSampleSize = inSampleSize;

        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    private void convertAndSend() {
        // Image location URL
        Log.e("path", "----------------" + picturePath);

        // Image
        Bitmap bm = BitmapFactory.decodeFile(picturePath);
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 90, bao);
        byte[] ba = bao.toByteArray();
        ba1 = Base64.encodeToString(ba, Base64.NO_WRAP);

        Log.e("base64", "-----" + ba1);


        Intent returnIntent = new Intent();
        returnIntent.putExtra("photodata", ba1);
        setResult(100, returnIntent);
        finish();

    }
}
