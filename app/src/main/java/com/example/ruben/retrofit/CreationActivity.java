package com.example.ruben.retrofit;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static com.example.ruben.retrofit.PhotoActivity.decodeSampledBitmapFromFile;

/**
 * Created by ruben on 17-4-2017.
 */

public class CreationActivity extends Activity {

    final String api = "https://stud.hosted.hr.nl/0889496/Jaar%203/parkright/";

    EditText titleField;
    EditText descriptionField;
    EditText photoField;
    EditText locationField;
    Button sendButton;

    String titleToCheck;
    String descriptionToCheck;
    String photoToCheck;
    String locationToCheck;

    String ba1;

    private TrackGPS gps;
    double longitude;
    double latitude;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creation);

        titleField = (EditText) findViewById(R.id.create_title);
        descriptionField = (EditText) findViewById(R.id.create_description);
        photoField = (EditText) findViewById(R.id.create_photo);
        locationField = (EditText) findViewById(R.id.create_location);
        sendButton = (Button) findViewById(R.id.btn_create);

        sendButton.setEnabled(true);

        try {

            /* Start of image intent code */

            Intent intent = getIntent();

            if (intent.hasExtra("image")) {

                File file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
                Bitmap bitmap2 = decodeSampledBitmapFromFile(file.getAbsolutePath(), 1000, 700);


                ByteArrayOutputStream bao = new ByteArrayOutputStream();
                bitmap2.compress(Bitmap.CompressFormat.JPEG, 90, bao);
                byte[] ba = bao.toByteArray();
                ba1 = Base64.encodeToString(ba, Base64.NO_WRAP);

                Log.d("BASE64_IMAGE", "CONTENT:    " + ba1);
                photoField.setEnabled(false);
                photoField.setText(ba1);

                /* End of image intent code */
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onCreateClick(View v) {
        checkValues();
    }

    public void checkValues() {
        titleToCheck = titleField.getText().toString();
        descriptionToCheck = descriptionField.getText().toString();
        photoToCheck = photoField.getText().toString();
        locationToCheck = locationField.getText().toString();

        if(!titleToCheck.isEmpty() || !descriptionToCheck.isEmpty() || !photoToCheck.isEmpty() || !locationToCheck.isEmpty() ) {
            sendButton.setEnabled(true);
            createEntry();
        } else {
            Toast.makeText(CreationActivity.this, "Vul alle velden in!", Toast.LENGTH_LONG).show();
        }
    }

    public void createEntry() {
        final String titleToSend = titleToCheck;
        final String descriptionToSend = descriptionToCheck;
        final String photoToSend = photoToCheck;
        final String locationToSend = locationToCheck;

        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest jsonObjRequest = new StringRequest(Request.Method.POST,
                api,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Toast.makeText(CreationActivity.this, "Versturen gelukt!", Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("volley", "Error: " + error.getMessage());
                Toast.makeText(CreationActivity.this, "Entry failed!", Toast.LENGTH_LONG).show();
                error.printStackTrace();

            }
        }) {

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("title", titleToSend);
                params.put("description", descriptionToSend);
                params.put("photo", photoToSend);
                params.put("location", locationToSend);

                return params;
            }
        };

        queue.add(jsonObjRequest);

        titleField.setText("");
        descriptionField.setText("");
        photoField.setText("");
        locationField.setText("");
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");

        file.delete();
    }

    public void onLocationCheckClick(View view) {
        onLocationClick();
    }

    public void onLocationClick () {

//        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
//
//        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(
//                    this,
//                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 124);
//        }


        gps = new TrackGPS(CreationActivity.this);

        longitude = gps.getLongitude();
        latitude = gps.getLatitude();

        String locationValue = Double.toString(latitude) + ", " + Double.toString(longitude);

        if(gps.canGetLocation()){

            locationField.setText(locationValue);
            Toast.makeText(getApplicationContext(),"Longitude:"+Double.toString(longitude)+"\nLatitude:"+Double.toString(latitude),Toast.LENGTH_SHORT).show();
        } else {

            gps.showSettingsAlert();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

            case 124:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    onLocationClick();
                } else {
                    Log.d("TAG", "Call Permission Not Granted");
                }
                break;

            default:
                break;
        }
    }

    public void onFinishClick(View view) {
        finish();
    }

    public void clickPic(View view) {
        Intent intent = new Intent(this, PhotoActivity.class);
        startActivityForResult(intent, 100);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        gps.stopUsingGPS();
    }

}
